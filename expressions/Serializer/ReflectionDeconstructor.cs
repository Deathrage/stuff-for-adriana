﻿using System.Reflection;

namespace Serializer;

public class ReflectionDeconstructor: IDeconstructor
{
    public IDictionary<string, object?> Deconstruct(object data)
    {
        Type dataType = data.GetType();

        Dictionary<string, object?> properties = dataType
            .GetProperties(BindingFlags.Public | BindingFlags.Instance)
            .Where(p => p.CanRead)
            .ToDictionary(p => p.Name, p => p.GetValue(data));
        
        Dictionary<string, object?> fields = dataType
            .GetFields(BindingFlags.Public | BindingFlags.Instance)
            .ToDictionary(f => f.Name, f => f.GetValue(data));
        
        return new Dictionary<string, object?>(properties.Concat(fields));
    }
}