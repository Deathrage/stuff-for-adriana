﻿namespace Serializer;

public interface IDeconstructor
{
    IDictionary<string, object?> Deconstruct(object data);
}