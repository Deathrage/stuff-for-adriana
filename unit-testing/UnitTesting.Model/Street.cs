﻿namespace UnitTesting.Model;

public class Street
{
    public  string Name { get; }

    public ICollection<House> Houses { get; }

    public Street(string name, ICollection<House> houses)
    {
        Name = name;
        Houses = houses;
    }
}