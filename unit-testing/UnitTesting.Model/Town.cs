﻿namespace UnitTesting.Model;

public class Town
{
    public string Name { get; }

    public ICollection<Street> Streets { get; }

    public Town(string name, ICollection<Street> streets)
    {
        Name = name;
        Streets = streets;
    }
}