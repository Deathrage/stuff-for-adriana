﻿namespace UnitTesting.Model;

public class House
{
    public string StreetNumber { get; }

    public ICollection<Person> Occupants { get; }
    
    public House(string streetNumber, ICollection<Person> occupants)
    {
        StreetNumber = streetNumber;
        Occupants = occupants;
    }
}