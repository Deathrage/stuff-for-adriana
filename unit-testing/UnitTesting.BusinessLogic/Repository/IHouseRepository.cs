﻿using UnitTesting.Model;

namespace UnitTesting.BusinessLogic.Repository;

public interface IHouseRepository
{
    House Get(string townName, string streeetName, string streetNumber);

    IEnumerable<House> GetAll();
}