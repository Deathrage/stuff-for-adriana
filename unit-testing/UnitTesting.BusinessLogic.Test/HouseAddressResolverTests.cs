﻿using Moq;
using NUnit.Framework;
using UnitTesting.BusinessLogic.Repository;
using UnitTesting.Model;

namespace UnitTesting.BusinessLogic.Test;

public class HouseAddressResolverTests
{
    [Test]
    public void Resolve()
    {
        // Setup
        Person testPersion = new(1, "Džonny Sinns", 10);
        House testHouse = new("50/10", [testPersion]);
        Street testStreet = new("Testovací ulice", [testHouse]);
        Town testTown = new("Testín", [testStreet]);

        Mock<IStreetRepository> streetRepository = new();
        streetRepository.Setup(_ => _.GetAll()).Returns([testStreet]);
        
        Mock<ITownRepository> townRepository = new();
        townRepository.Setup(_ => _.GetAll()).Returns([testTown]);

        HouseAddressResolver sut = new(streetRepository.Object, townRepository.Object);

        // Act
        string? result = sut.Resolve(testHouse);
        
        // Assert
        Assert.That(result, Is.EqualTo("Testovací ulice 50/10, Testín"));
    }

    [Test]
    public void Resolve_MultipleStreets()
    {
        // Setup
        Person testPersion = new(1, "Džonny Sinns", 10);
        House testHouse = new("50/10", [testPersion]);
        Street testStreet1 = new("Testovací avenue", [testHouse]);
        Street testStreet2 = new("Testovací ulička", [testHouse]);
        Town testTown = new("Testín", [testStreet1, testStreet2]);

        Mock<IStreetRepository> streetRepository = new();
        streetRepository.Setup(_ => _.GetAll()).Returns([testStreet1, testStreet2]);

        Mock<ITownRepository> townRepository = new();
        townRepository.Setup(_ => _.GetAll()).Returns([testTown]);

        HouseAddressResolver sut = new(streetRepository.Object, townRepository.Object);

        // Act and ssert
        Assert.Throws<InvalidOperationException>(() => sut.Resolve(testHouse));
    }

    [Test]
    public void Resolve_StreetlessHouse()
    {
        // Setup
        Person testPersion = new(1, "Džonny Sinns", 10);
        House testHouse = new("50/10", [testPersion]);
        Street testStreet = new("Testovací ulice", []);
        Town testTown = new("Testín", [testStreet]);

        Mock<IStreetRepository> streetRepository = new();
        streetRepository.Setup(_ => _.GetAll()).Returns([testStreet]);

        Mock<ITownRepository> townRepository = new();
        townRepository.Setup(_ => _.GetAll()).Returns([testTown]);

        HouseAddressResolver sut = new(streetRepository.Object, townRepository.Object);


        // Act
        string? result = sut.Resolve(testHouse);

        // Assert
        Assert.That(result, Is.Null);
    }

    [Test]
    public void Resolve_TownlessStreet()
    {
        // Setup
        Person testPersion = new(1, "Džonny Sinns", 10);
        House testHouse = new("50/10", [testPersion]);
        Street testStreet = new("Testovací ulice", [testHouse]);
        Town testTown = new("Testín", []);

        Mock<IStreetRepository> streetRepository = new();
        streetRepository.Setup(_ => _.GetAll()).Returns([testStreet]);

        Mock<ITownRepository> townRepository = new();
        townRepository.Setup(_ => _.GetAll()).Returns([testTown]);

        HouseAddressResolver sut = new(streetRepository.Object, townRepository.Object);
        
        // Act
        string? result = sut.Resolve(testHouse);

        // Assert
        Assert.That(result, Is.Null);
    }
}