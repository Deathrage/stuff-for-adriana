﻿using NUnit.Framework;
using UnitTesting.Model;

namespace UnitTesting.BusinessLogic.Test;

public class EmptyHouseFinderTests
{
    [Test]
    public void Find()
    {
        // Setup
        Person person1 = new(1, "Testman", 90);
        House houseWithPerson = new("10", [person1]);
        House emptyHouse = new("11", []);
        Street street1 = new("Balikova", [houseWithPerson, emptyHouse]);
        Town town1 = new("Balikov", [street1]);

        EmptyHouseFinder sut = new();

        // Act
        House[] result = sut.Find(town1).ToArray();

        // Assert
        Assert.That(result, Is.EquivalentTo(new[] { emptyHouse }));
    }
}