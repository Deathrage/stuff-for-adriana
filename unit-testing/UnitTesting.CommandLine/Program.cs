﻿
using Microsoft.Extensions.DependencyInjection;
using UnitTesting.BusinessLogic;
using UnitTesting.BusinessLogic.Repository;
using UnitTesting.CommandLine;
using UnitTesting.Model;

Town klidneMestecko = new("Klidné Městečko", [
    new("Masarykova ulice", [
        new("1", [
            new(1, "Velká Unce", 32),
            new(2, "Velký Čungus", 40),
            new(3, "Malý Čungus", 10)
        ]),
        new("2", [
            new(4, "Arčibald Balík", 10),
        ]),
    ]),
    new("Benešova ulice", [
        new("3", []),
    ]),
    new("Háchova ulice", [
        new("4", [
            new(5, "Danil Tokarev", 70),
        ]),
        new("5", [
            new(6, "Lanžhot Tokarev", 65),]),
        new("6", []),
    ]),
]);

IServiceProvider serviceProvider = new ServiceCollection()
    .AddSingleton(new StaticDataRepository([klidneMestecko]))
    .AddSingleton<ITownRepository>(serviceProvider => serviceProvider.GetRequiredService<StaticDataRepository>())
    .AddSingleton<IStreetRepository>(serviceProvider => serviceProvider.GetRequiredService<StaticDataRepository>())
    .AddSingleton<IHouseRepository>(serviceProvider => serviceProvider.GetRequiredService<StaticDataRepository>())
    .AddSingleton<IPersonRepository>(serviceProvider => serviceProvider.GetRequiredService<StaticDataRepository>())
    .AddSingleton<EmptyHouseFinder>()
    .AddSingleton<HouseAddressResolver>()
    .AddSingleton<PersonFinder>()
    .BuildServiceProvider();

Console.WriteLine($"Město {klidneMestecko.Name} obsahuje adresy:");
IHouseRepository houseRepository = serviceProvider.GetRequiredService<IHouseRepository>();
HouseAddressResolver houseAddressResolver = serviceProvider.GetRequiredService<HouseAddressResolver>();
foreach (House house in houseRepository.GetAll())
{
    string? address = houseAddressResolver.Resolve(house);
    if (address is not null)
        Console.WriteLine(address);
}

WriteSplit();

Console.WriteLine($"Ve městě {klidneMestecko.Name} žijí tyto dospělé osoby:");
PersonFinder personFinder = serviceProvider.GetRequiredService<PersonFinder>();
foreach (Person adult in personFinder.FindAdults(klidneMestecko))
    Console.WriteLine(adult.Name);

WriteSplit();

Console.WriteLine($"Ve městě {klidneMestecko.Name} žijí tyto opuštěné děti:");
foreach (Person adult in personFinder.FindUnaccompaniedChildren(klidneMestecko))
    Console.WriteLine(adult.Name);

WriteSplit();

Console.WriteLine($"Ve městě {klidneMestecko.Name} jsou tyto volné baráky:");
EmptyHouseFinder emptyHouseFinder = serviceProvider.GetRequiredService<EmptyHouseFinder>();
foreach (House emptyHouse in emptyHouseFinder.Find(klidneMestecko))
{
    string? address = houseAddressResolver.Resolve(emptyHouse);
    if (address is not null)
        Console.WriteLine(address);
}

void WriteSplit()
    => Console.WriteLine("-------------------");